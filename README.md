# LaTeX Examples

This repo contains some code examples for the LaTeX session in resbaz 2019.

## Files

There are four example files with two supporting files. 
1. Example01_HelloWorld.tex is a "Hello World" equivalent in LaTeX. 
2. Example02_Basic.tex gives the basic introduction to LaTeX. 
3. Example03_TablesFigures.tex focus on creating Tables and Figures in LaTeX. It requires Perth.jpg in the same directory. 
4. Example04_bibtex.tex demonstrates BibTeX, a separate program that generates references. It requires the bib file ch12.bib in the same directory. 
5. main.tex provides an example on how to compile a (potentially) large document using multiple tex source. This is to be used with section01.tex and extra_table.tex
6. section01.tex is a supplementary source to main.tex. 
7. extra_table.tex is a supplementary source to main.tex. 

## Demonstration

The plan is to provide some explanation and demonstrate the compilatin of these files during the session. I will demonstrate the compilation via Overleaf but these files should also work locally on your computer with a LaTeX installation.  
